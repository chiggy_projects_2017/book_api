<?php

require __DIR__.'/vendor/autoload.php';

use Guzzle\Http\Client;

// create our http client (Guzzle)
$client = new Client('http://localhost:8000', array(
    'request.options' => array(
        'exceptions' => false,
    )
));

// Create data to insert
$title = 'Book'.rand(0, 999); // creates unique book each time I run a post request
// data to be posted
$data = [
    'title' => $title,
    'author' => 'Chigbo',
    'price' => 25,
    'publisher' => 'Johnson and Johnson publishers',
    'format' => 'Kindle'
];


// POST Request - to create a book resource
$request = $client->post('/api/books',null, json_encode($data)); //encode the data
$response = $request->send();
$bookUrl = $response->getHeader('Location');

echo $response;
echo "\n\n";

// GET Request - to get a book resource
$request = $client->get($bookUrl); //'/api/books/'.$id
$response = $request->send();

echo $response;
echo "\n\n";

// Get Request - to get all books resource
$request = $client->get('/api/books');
$response = $request->send();

echo $response;
echo "\n\n";

// Put Request - to update/modify a book resource
$data1 = [
    'title' => 'Book42',
    'author' => 'Chigbo New',
    'price' => 95,
    'publisher' => 'New Publishers',
    'format' => 'Paper Back'
];
$request = $client->put($bookUrl, null, json_encode($data1));
$response = $request->send();

echo $response;
echo "\n\n";

// Delete Request - to delete a book resource
$request = $client->delete($bookUrl);
$response = $request->send();

// display the response value
echo $response;
echo "\n\n";
die;