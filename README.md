book_api
=================

A Symfony project created on April 8, 2019, 12:21 am.


### Setup Instructions ###

1.  Run 'composer install' to install all required packages and thier dependencies

2.  Set up your server - you could use Mamp if you are on a mac or  Xammp if you are using a windows machine.

3.  Change the database config details in app/config/parameters.yml to match yours.

4.  Use doctrine command to create database and table:
    php bin/console doctrine:database:create
    php bin/console doctrine:schema:update --force
    or
    You can run the commands in book.sql to create our database and book table

5.  Use following endpoints to insert, fetch, update or delete data from your database
    /api/books  - insert data. You would require to specify the data (json) to insert in postman body
    /api/books/1 - fetch a book. The value 1 can be changed as desired
    /api/books  - fetches all books
    /api/books/1  - delete a book. The value 1 can be changed as desired
    /api/books/1  - update a book. You would require to specify the data (json) to insert in postman body

    Note: You can use postman to test the endpoints. And remember to include host and port in the uri.

6. To run tests type the following command in terminal
    ./vendor/bin/phpunit

7. To read about what was done and areas that could be improved if given more time checkout
     Dev Challange documment.docx

     #./bin/console doctrine:fixtures:load - to load fixtures