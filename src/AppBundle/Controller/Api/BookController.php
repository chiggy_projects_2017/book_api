<?php
/**
 * Created by PhpStorm.
 * User: onyinyechukwunnadi-chigbo
 * Date: 06/04/19
 * Time: 19:41
 */

namespace AppBundle\Controller\Api;

use AppBundle\Repository\BookEntityManager;
use AppBundle\Repository\BookRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class BookController extends Controller
{

    /**
     * @var BookRepository
     */
    private $bookRepository;

    /**
     * @var BookEntityManager
     */
    private $bookEntityManager;


    /**
     * @param BookRepository $bookRepository
     */
    public function __construct(BookRepository $bookRepository, BookEntityManager $bookEntityManager)
    {
        $this->bookRepository = $bookRepository;
        $this->bookEntityManager = $bookEntityManager;
    }

    /**
     * Creates a Resource in db
     * @Route("/api/books")
     * @Method("POST")
     * @return Response
     * @param $request
     */
    public function newAction(Request $request) {
        // Get the request body and decode it into an array
        $body = $request->getContent();
        $data = json_decode($body, true);

        // Create book
        $book = $this->bookRepository->createBook($data);

        // save data to database
        /*$em = $this->getDoctrine()->getManager();
        $em->persist($book);
        $em->flush();*/
        $this->bookEntityManager->save($book);

        // Get Url
        $url = $this->generateUrl('api_books_show', [
            'id' => $book->getId()
        ]);

        // Data to use for returning representation of the resource - saves user from making one more request
        $data = $this->bookRepository->serializeBooks($book);

        // Encode json string, set status to 201 and set conten type to json
        $response = new JsonResponse($data, 201);

        // Set header location and content type
        $response->headers->set('Location', $url);

        // Return Response
        return  $response;

    }

    /**
     * Returns a single book
     * @Route("/api/books/{id}", name="api_books_show")
     * @Method("GET")
     * @param $id
     * @return Response
     */
    public function showAction($id)
    {
        // Query database to get a record
        $book = $this->bookRepository->findBookById($id);

        // Check if book exists - if it doesn't throw an exception
        if (!$book) {
            throw $this->createNotFoundException('No product found for id ' . $id);
        }

        // Get record details
        $data = $this->bookRepository->serializeBooks($book);

        // Encode json string, set status to 200 and set content type to json
        $response = new JsonResponse($data, 200);

        // Return book as response
        return $response;

    }

    /**
     * Creates collection of book resource
     * @Route("/api/books")
     * @Method("GET")
     * @return Response
     */
    public function ListAction() {
        // Query database to get all records
        $books = $this->bookRepository->findAllBooks();

        // Loop through book object
        $data = ['books' => []];
        foreach ($books as $book) {
            $data['books'][] = $this->bookRepository->serializeBooks($book);
        }

        // Encode json string, set status to 200 and set content type to json
        $response = new JsonResponse($data, 200);

        // Return collection of books as response
        return $response;

    }

    /**
     * Updates a record
     * @Route("/api/books/{id}", name="api_books_update")
     * @Method("PUT")
     * @param $id
     * @param $request
     * @return Response
     */
    public function updateAction(Request $request, $id) {
        // Get the request body and decode it into an array
        $body = $request->getContent();
        $data = json_decode($body, true);

        // Query database to get a record
        $book = $this->bookRepository->findBookById($id);


        // Check if book exists - if it doesn't throw an exception
        if (!$book) {
            throw $this->createNotFoundException('No product found for id ' . $id);
        }

        // Set record fields to json data
        $setFields = $this->bookRepository->setFields($book, $data);

        // save data to database
        /*$em = $this->getDoctrine()->getManager();
        $em->persist($book);
        $em->flush();*/
        $this->bookEntityManager->save($book);

        // Data to use for returning representation of the resource
        $data = $this->bookRepository->serializeBooks($setFields);

        // Encode json string, set status to 201 and set conten type to json
        $response = new JsonResponse($data, 200);

        // Return Response
        return  $response;

    }


    /**
     * Deletes a record
     * @Route("/api/books/{id}", name="api_books_delete")
     * @Method("DELETE")
     * @param $id
     * @return Response
     */
    public function deleteAction($id) {
        // Query database to get a record
        $book = $this->bookRepository->findBookById($id);

        // Delete book from db it exists
        if ($book) {
            /*$em = $this->getDoctrine()->getManager();
            $em->remove($book);
            $em->flush();*/
            $this->bookEntityManager->delete($book);
        }

        // Return Response
        return $response = new Response(null,204);
    }

}
