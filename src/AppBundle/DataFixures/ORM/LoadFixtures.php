<?php
/**
 * Created by PhpStorm.
 * User: onyinyechukwunnadi-chigbo
 * Date: 11/04/19
 * Time: 05:46
 */

namespace AppBundle\DataFixures\ORM;


use AppBundle\Entity\Book;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Nelmio\Alice\Fixtures;

class LoadFixtures extends Fixture
{
    public function load(ObjectManager $manager) {
        /*$book = new Book();
        $book->setTitle('Book'.rand(0, 999));
        $book->setAuthor('Sandra'.rand(0, 999));
        $book->setPrice(rand(100, 9999));
        $book->setPublisher('Johnson and Jhonson'.rand(0, 999));
        $book->setFormat('Kindle'. rand(0, 999));

        $manager->persist($book);
        $manager->flush();

        */

        Fixtures::load(
            __DIR__.'/fixtures.yml',
            $manager,
            [
                'providers' => [$this]
            ]

        );

    }

    public function book() {
        $bookGenerator = [
            'Clean coder',
            'Software Architecture',
            'Test Driven development',
            'ReactJs',
            'Nodejs',
            'Jquery',
            'Angular js'
        ];

        $key = array_rand($bookGenerator);
        return $bookGenerator[$key];
    }
}