<?php

namespace AppBundle\Test;

use Guzzle\Http\Client;

class ApiTestCase extends \PHPUnit_Framework_TestCase
{
    private  static $staticClient;

    /**
     * @var Client
     */
    protected $client;

    public static function setUpBeforeClass()
    {
        // create our http client (Guzzle)
        self::$staticClient = new Client('http://localhost:8000', array(
            'request.options' => array(
                'exceptions' => false,
            )
        ));
    }

    public function setup()
    {
        $this->client = self::$staticClient;
    }


}
