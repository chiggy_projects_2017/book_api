<?php
/**
 * Created by PhpStorm.
 * User: onyinyechukwunnadi-chigbo
 * Date: 10/04/19
 * Time: 19:52
 */

namespace AppBundle\Repository;


use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use AppBundle\Entity\Book;

class BookRepository extends EntityRepository
{
    /**
     * Create a book
     * @param $bookData
     * @return Book
     */
    public function createBook($bookData)
    {
        $book = new Book();
        return $this->setFields($book, $bookData);
    }


    /**
     * Function that returns an array of books
     * @param Book $book
     * @return array
     */
    public function serializeBooks (Book $book) {
        return [
            'id' => $book->getId(),
            'title' => $book->getTitle(),
            'author' => $book->getAuthor(),
            'price' => $book->getPrice(),
            'publisher' => $book->getPublisher(),
            'format' => $book->getFormat(),
        ];
    }

    /**
     * Set Book fields to data from json
     * @param Book $book
     * @param $data
     * @return Book
     */
    public function setFields (Book $book, $data){
        $book->setTitle($data['title']);
        $book->setFormat($data['format']);
        $book->setPrice($data['price']);
        $book->setPublisher($data['publisher']);
        $book->setAuthor($data['author']);

        return $book;
    }

    /**
     * Fetch a book by id
     * @param $id
     * @return object
     */
    public function findBookById($id) {
        return $this->findOneBy([
            'id' => $id
        ]);
    }

    /**
     * Fetch all books
     * @return array
     */
    public function findAllBooks() {
        return $this->findAll();
    }
}