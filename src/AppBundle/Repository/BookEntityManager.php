<?php
/**
 * Created by PhpStorm.
 * User: onyinyechukwunnadi-chigbo
 * Date: 12/04/19
 * Time: 14:31
 */

namespace AppBundle\Repository;

use Doctrine\ORM\EntityManagerInterface;

class BookEntityManager

{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * Save a book
     * @param $book
     */
    public function save($book) {
        $this->em->persist($book);
        $this->em->flush();
    }

    /**
     * Delete a book
     * @param $book
     */
    public function delete($book) {
        $this->em->remove($book);
        $this->em->flush();
    }

}