<?php
/**
 * Created by PhpStorm.
 * User: onyinyechukwunnadi-chigbo
 * Date: 07/04/19
 * Time: 22:23
 */

namespace AppBundle\Tests\Controller\Api;

use AppBundle\Test\ApiTestCase;

class BookControllerTest extends ApiTestCase
{
   // Test - POST request
   public function testNewAction() {
       // data to be posted
       $data = [
           'id' => 5,
           'title' => 'Clean coder',
           'author' => 'Chigbo',
           'price' => 25,
           'publisher' => 'Johnson and Johnson publishers',
           'format' => 'Kindle'
       ];

       // POST Request - to create a book resource
       $request = $this->client->post('/api/books',null, json_encode($data));
       $response = $request->send();

       // Assertions
       $this->assertEquals(201, $response->getStatusCode());
       $this->assertTrue($response->hasHeader('Location'));
       $this->assertEquals('application/json', $response->getContentType());
       $finishedData = json_decode($response->getBody(), true);
       $this->assertArrayhasKey('title', $finishedData);
       $this->assertEquals(5, $data['id']);
       $this->assertEquals('/api/books/5' , $response->getHeader('Location'));

   }

  /*// Test - GET request (one record)
   public function testShowData() {
       $bookUrl = '/api/books/1';

       // GET Request - to get a book resource
       $request = $this->client->get($bookUrl); //'/api/books/'.$id
       $response = $request->send();

       // Assertions
       $this->assertEquals(200, $response->getStatusCode());
       $this->assertEquals('application/json', $response->getContentType());
   }

   // Test - PUT request
   public function testUpdateAction() {
       // Record to modify
       $bookUrl = '/api/books/1';
       $data = [
           'title' => 'New Book title',
           'author' => 'Chigbo New',
           'price' => 95,
           'publisher' => 'New Publishers',
           'format' => 'Paper Back'
       ];

       // PUT Request - to modify a book resource
       $request = $this->client->put($bookUrl, null, json_encode($data));
       $response = $request->send();

       // Assertions
       $this->assertEquals(200, $response->getStatusCode());
       $this->assertEquals('application/json', $response->getContentType());
       $finishedData = json_decode($response->getBody(), true);
       $this->assertArrayhasKey('title', $finishedData);

   }

   // Test - DELETE request
   public function testDeleteAction() {
       // Record to delete
       $bookUrl = '/api/books/1';

       // DELETE Request - to delete a book resource
       $request = $this->client->delete($bookUrl);
       $response = $request->send();

       // Assertion
       $this->assertEquals(204, $response->getStatusCode());
   }
  */

}
